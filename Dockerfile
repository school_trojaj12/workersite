FROM python:3.8

RUN mkdir /app
COPY . /app
RUN pip3 install -r /app/requirements.txt && rm /app/requirements.txt 

WORKDIR /app
ENTRYPOINT [ "python3", "manage.py", "runserver" ]
CMD [ "" ]